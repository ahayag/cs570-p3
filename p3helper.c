/*Program 3
 * Aaron Hayag 817898933
 * Dr. John Carroll
 * CS 570
 *
 * p3helper.c is used to coordinate the robots to place widgets in a specific "triangle"
 * pattern. Two routines included here is initStudentStuff() and placeWidget().
 * initStudentStuff() is used to initialize semaphores and shared files that the robots
 * will be using. placeWidget() is where robots access and updates the shared files to
 * ensure that they place the widgets the right way. The formula I used to coordinate the
 * robots will be explained in gradernotes.
 */


/* p3helper.c
   Program 3 assignment
   CS570
   SDSU
   Fall 2019

   This is the ONLY file you are allowed to change. (In fact, the other
   files should be symbolic links to
     ~cs570/Three/p3main.c
     ~cs570/Three/p3robot.c
     ~cs570/Three/p3.h
     ~cs570/Three/makefile
     ~cs570/Three/CHK.h    )
   */
#include "p3.h"
#include <math.h>

/* You may put declarations/definitions here.
   In particular, you will probably want access to information
   about the job (for details see the assignment and the documentation
   in p3robot.c):
     */
extern int nrRobots;
extern int quota;
extern int seed;

int countfd; //file descriptor for the shared count file
sem_t *pmutx; //this is a mutex to ensure that there is mutual exclusion when accessing the shared conut file
int count; //local copy of counter

int shrinkWherefd; //fd for shared chrinkWhere file
int squareNum = 0; //will be used to find the next square number ie: 1, 4, 9, 16, etc. (see gradernotes for explanation why)
int shrinkWhere; //when to start shrinking

char semaphoreMutx[SEMNAMESIZE]; 

int howManyfd; //fd for shared howManyfile
int howMany; //will be used to increment pascal (see gradernotes for explanation why)

int pascalfd; //fd for shared pascalfile
int pascal; //will hold a pascal value (will be used to figure out when to print "N\n")

int shrinkBoolfd; //fd for shared shrinkBoolfile
int shrinkBool; //if 0, increase row. if 1 decrease row

/* General documentation for the following functions is in p3.h
   Here you supply the code, and internal documentation:
   */
void initStudentStuff(void) {
	//initialization of any data structures including semaphores that the cooperating robots need

	sprintf(semaphoreMutx,"%s%ldmutx",COURSEID,(long)getuid()); //try to make a "name" for semaphore that will not clash
	
	//create and initialize the pmutx semaphore
	
	//we first check if the pmutx semaphore has been created, if not then the first robot has to create it. else the following robots only need to open it
	//since this is a mutex(binary semaphore) we initialize its value to 1 so it will work as a lock and only allow one robot to the count file each time
	if(SEM_FAILED != (pmutx = sem_open(semaphoreMutx,O_RDWR|O_CREAT|O_EXCL,S_IRUSR|S_IWUSR,1))){
		//first robot creates and initializes all the shared files

		CHK(sem_wait(pmutx)); //this will cause other robots to wait for countfile to be created before trying to open it and cause a no file exists error
		CHK(countfd = open("/home/cs/carroll/cssc0048/Three/countfile",O_RDWR|O_CREAT|O_TRUNC,S_IRUSR|S_IWUSR)); //use full path instead of just creating in
															 //whatever the current directory is
		count = 0;
		CHK(lseek(countfd,0,SEEK_SET));
		assert(sizeof(count) == write(countfd,&count,sizeof(count)));
		
		CHK(shrinkWherefd = open("/home/cs/carroll/cssc0048/Three/shrinkWherefile",O_RDWR|O_CREAT|O_TRUNC,S_IRUSR|S_IWUSR));
		//this loop helps us find the next square number. ex) nrRobots * quota = 19. next square number is squareNum = 25
		//thus, shrinkWhere = sqrt(25) = 5. this will help us determine when to start shrinking
		int oddNum = 1;
		while(squareNum < (nrRobots * quota)){
			squareNum += oddNum;
			oddNum += 2;
		}
		shrinkWhere = sqrt(squareNum);
		CHK(lseek(shrinkWherefd,0,SEEK_SET));
		assert(sizeof(count) == write(shrinkWherefd,&shrinkWhere,sizeof(shrinkWhere)));

		//initialize howManyfile
		CHK(howManyfd = open("/home/cs/carroll/cssc0048/Three/howManyfile",O_RDWR|O_CREAT|O_TRUNC,S_IRUSR|S_IWUSR));
		howMany = 1;
		CHK(lseek(howManyfd,0,SEEK_SET));
		assert(sizeof(howMany) == write(howManyfd,&howMany,sizeof(howMany)));
		
		//initialize pascalfile
		CHK(pascalfd = open("/home/cs/carroll/cssc0048/Three/pascalfile",O_RDWR|O_CREAT|O_TRUNC,S_IRUSR|S_IWUSR));
		pascal = 1;
		CHK(lseek(pascalfd,0,SEEK_SET));
		assert(sizeof(pascal) == write(pascalfd,&pascal,sizeof(pascal)));
		
		//initialize shrinkBoolfile
		CHK(shrinkBoolfd = open("/home/cs/carroll/cssc0048/Three/shrinkBoolfile",O_RDWR|O_CREAT|O_TRUNC,S_IRUSR|S_IWUSR));
		shrinkBool = 0;
		CHK(lseek(shrinkBoolfd,0,SEEK_SET));
		assert(sizeof(shrinkBool) == write(shrinkBoolfd,&shrinkBool,sizeof(shrinkBool)));

		CHK(sem_post(pmutx)); //files have been initialized other robots can now open it
	}
	else{ 
		//following robots only need to open pmutx semaphore and shared files
		CHK(SEM_FAILED != (pmutx = sem_open(semaphoreMutx,O_RDWR)));
		CHK(sem_wait(pmutx)); //wait for first robot to create countfile first
		CHK(countfd = open("/home/cs/carroll/cssc0048/Three/countfile",O_RDWR));	
		CHK(shrinkWherefd = open("/home/cs/carroll/cssc0048/Three/shrinkWherefile",O_RDWR));
		CHK(howManyfd = open("/home/cs/carroll/cssc0048/Three/howManyfile",O_RDWR));
		CHK(pascalfd = open("/home/cs/carroll/cssc0048/Three/pascalfile",O_RDWR));
		CHK(shrinkBoolfd = open("/home/cs/carroll/cssc0048/Three/shrinkBoolfile",O_RDWR));

		CHK(sem_post(pmutx));
	}

}

/* In this braindamaged version of placeWidget, the widget builders don't
   coordinate at all, and merely print a random pattern. You should replace
   this code with something that fully follows the p3 specification. */
void placeWidget(int n) {
	CHK(sem_wait(pmutx)); //request access to critical section
	
	//begin critical section
	CHK(lseek(countfd,0,SEEK_SET));
	assert(sizeof(count) == read(countfd,&count,sizeof(count))); //read count
	count++; //increment count
	CHK(lseek(countfd,0,SEEK_SET));
	assert(sizeof(count) == write(countfd,&count,sizeof(count))); //write count

	CHK(lseek(shrinkWherefd,0,SEEK_SET));
	assert(sizeof(shrinkWhere) == read(shrinkWherefd,&shrinkWhere,sizeof(shrinkWhere))); //read shrinkWhere

	CHK(lseek(howManyfd,0,SEEK_SET));
	assert(sizeof(howMany) == read(howManyfd,&howMany,sizeof(howMany))); //read howMany

	CHK(lseek(pascalfd,0,SEEK_SET));
	assert(sizeof(pascal) == read(pascalfd,&pascal,sizeof(pascal))); //read pascal

	CHK(lseek(shrinkBoolfd,0,SEEK_SET));
	assert(sizeof(shrinkBool) == read(shrinkBoolfd,&shrinkBool,sizeof(shrinkBool))); //read shrinkBool

	if(count == (nrRobots * quota)){ //this is the last widget, we have to print "F\n" and close and unlink files and semaphore
					 //we have to unlink so that in the next consecutive p3 run the files are removed and we can create them again
		printeger(n);
		printf("F\n");
		fflush(stdout);
		CHK(sem_close(pmutx));
		CHK(sem_unlink(semaphoreMutx));
		CHK(close(countfd));
		CHK(unlink("/home/cs/carroll/cssc0048/Three/countfile"));
		CHK(close(shrinkWherefd));
		CHK(unlink("/home/cs/carroll/cssc0048/Three/shrinkWherefile"));
		CHK(close(howManyfd));
		CHK(unlink("/home/cs/carroll/cssc0048/Three/howManyfile"));
		CHK(close(pascalfd));
		CHK(unlink("/home/cs/carroll/cssc0048/Three/pascalfile"));
		CHK(close(shrinkBoolfd));
		CHK(unlink("/home/cs/carroll/cssc0048/Three/shrinkBoolfile"));
	}
	else if(count == pascal){ //we print "N\n" on pascal values (this is explained further in gradernotes)
		printeger(n);
		printf("N\n");
		fflush(stdout);

		if(howMany == shrinkWhere){ //we need to start shrinking
			shrinkBool++; //shrinkBool is now set to 1 so that program nows that we are in "shrinking" mode.
			CHK(lseek(shrinkBoolfd,0,SEEK_SET));
			assert(sizeof(shrinkBool) == write(shrinkBoolfd,&shrinkBool,sizeof(shrinkBool)));
		}
		if(shrinkBool == 0){ //shrinkBool is set to 0, howMany is always incremented
			howMany++;
		}
		else{ //shrinkBool is set to 1, howMany is always decremented
			howMany--;
		}

		CHK(lseek(howManyfd,0,SEEK_SET));
		assert(sizeof(howMany) == write(howManyfd,&howMany,sizeof(howMany)));

		pascal += howMany; //pascal is incremented by howMany. this is where the "magic" happens. if howMany is constantly incremented by 1, then
                                   //the rows will appear increasing. however if howMany is constantly decremented by 1, then the rows will start shrinking
                                   //(see gradernotes for full explanation).
		CHK(lseek(pascalfd,0,SEEK_SET));
		assert(sizeof(pascal) == write(pascalfd,&pascal,sizeof(pascal)));
		
		CHK(sem_post(pmutx));
	}
	else{	
		printeger(n);
		fflush(stdout);
		
		//end critical section
		CHK(sem_post(pmutx)); //release critical section
	}
	
}

/* If you feel the need to create any additional functions, please
   write them below here, with appropriate documentation:
   */
